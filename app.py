import datetime

import dash
from dash.dependencies import Input, Output, State
import dash_core_components as dcc
import dash_html_components as html

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.scripts.config.serve_locally = True

app.layout = html.Div([
    dcc.Upload(
        id='upload-image',
        children=html.Div([
            'Drag and Drop or ',
            html.A('Select Files')
        ]),
        style={
            'width': 900,
            'height': '60px',
            'lineHeight': '60px',
            'borderWidth': '1px',
            'borderStyle': 'dashed',
            'borderRadius': '5px',
            'textAlign': 'center',
            'margin': '10px'
        },
        # do not Allow multiple files to be uploaded
        multiple=True
    ),
    dcc.Dropdown(
        id='my-dropdown',
        options=[
            {'label': 'a', 'value': 'a'},
            {'label': 'b', 'value': 'b'},
            {'label': 'c', 'value': 'c'},
            {'label': 'd', 'value': 'd'},
            {'label': 'e', 'value': 'e'},
            {'label': 'f', 'value': 'f'},
            {'label': 'g', 'value': 'g'},
            {'label': 'h', 'value': 'h'},
            {'label': 'i', 'value': 'i'},
            {'label': 'j', 'value': 'j'},
            {'label': 'k', 'value': 'k'},
            {'label': 'l', 'value': 'l'},
            {'label': 'm', 'value': 'm'},
            {'label': 'n', 'value': 'n'},
            {'label': 'o', 'value': 'o'},
            {'label': 'p', 'value': 'p'},
            {'label': 'q', 'value': 'q'},
            {'label': 'r', 'value': 'r'},
            {'label': 's', 'value': 's'},
            {'label': 't', 'value': 't'},
            {'label': 'u', 'value': 'u'},
            {'label': 'v', 'value': 'v'},
            {'label': 'w', 'value': 'w'},
            {'label': 'x', 'value': 'x'},
            {'label': 'y', 'value': 'y'},
            {'label': 'z', 'value': 'z'}                                                                                              
        ],
        style={
            'width': 100
        }
    ),
    html.Div(
        id='output-image-upload',
    ),

])


def parse_contents(contents, filename, date, dropdown):
    return html.Div([
        html.H5('FILE: ' + filename),
        html.H6('TIME: ' + str(datetime.datetime.fromtimestamp(date))),
        html.H6('LABEL: ' + dropdown),

        # HTML images accept base64 encoded strings in the same format
        # that is supplied by the upload
        html.Img(
            src=contents,
            style={
                'width': 450,
                'height': 400
            }
        ),
        html.Hr(),
        html.Div('Raw Content'),
        html.Pre(
            contents[0:200] + '...', 
            style={
                'whiteSpace': 'pre-wrap',
                'wordBreak': 'break-all'
        })
    ])


@app.callback(Output('output-image-upload', 'children'),
              [Input('upload-image', 'contents')],
              [State('upload-image', 'filename'),
              State('upload-image', 'last_modified'),
              State('my-dropdown', 'value')])
def update_output(list_of_contents, list_of_names, list_of_dates, dropdown_value):
    if list_of_contents is not None:

        if dropdown_value is not None:

            children = [
                parse_contents(c, n, d, dd) for c, n, d, dd in
                zip(list_of_contents, list_of_names, list_of_dates, [dropdown_value])
            ]
            return children



if __name__ == '__main__':
    app.run_server(debug=True, host='0.0.0.0', port=80)

# import dash
# import dash_core_components as dcc
# import dash_html_components as html

# app = dash.Dash()
# application = app.server

# app.layout = html.Div(children=[
#     html.H1(children='Hello Dash'),

#     html.Div(children='''
#         This is Dash running on Azure App Service.
#     '''),

#     dcc.Graph(
#         id='example-graph',
#         figure={
#             'data': [
#                 {'x': [1, 2, 3], 'y': [4, 1, 2], 'type': 'bar', 'name': 'SF'},
#                 {'x': [1, 2, 3], 'y': [2, 4, 5], 'type': 'bar', 'name': u'Montréal'},
#             ],
#             'layout': {
#                 'title': 'Dash Data Visualization'
#             }
#         }
#     )
# ])

# if __name__ == '__main__':
#     application.run(debug=True, host='0.0.0.0', port='80')










